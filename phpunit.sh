#!/bin/bash
wget -O bin/phpunit.phar https://phar.phpunit.de/phpunit-9.5.phar > /dev/null 2>&1
chmod +x bin/phpunit.phar
if [ ! -f bin/phpunit ]; then
  cd bin
  ln -sn phpunit.phar phpunit
  cd ..
fi
bin/phpunit --version
